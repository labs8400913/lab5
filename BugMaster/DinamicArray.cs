﻿using System;
using System.Collections;
namespace ArrayLibLAB5
{
    public class DinamicArray<T> where T: IEnumerable, IComparable<T>
    {
        T[] Array;
        public int Count { private set; get; }
        public int Capacity {set;get;}

        public DinamicArray()
        {
            Capacity = 20;
            Count = 0;
            Array = new T[20];
        }
        public DinamicArray(int cap)
        {
            Capacity = cap;
            Count = 0;
            Array = new T[Capacity];
        }

        public void Add(T element)
        {
            if (Count  <= Capacity)
            {
                
                this.Array[Count] = element;
                Count++;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Выход за пределы ёмкости");
            }

        }
        //public void Add(IEnumerable<T> elements)
        //{

        //}
        public void Insert(T element, int position)
        {
            if(Count+1<=Capacity)
            {
                T[] tempArr = new T[Capacity];
                int j = 0;
                for (int i = 0; i < Count; i++)
                {

                    if (i==position)
                    {
                        tempArr[i] = element;
                        j--;
                    }
                    else
                    {
                        tempArr[i] = this.Array[j];
                    }
                    j++;
                }
                this.Array = tempArr;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Выход за пределы ёмкости");
            }

        }
        public void RemoveAt(int position)
        {
            if (Count - 1 < 0)
            {
                T[] tempArr = new T[Capacity];
                int j = 0;
                for (int i = 0; i < Count; i++)
                {

                    if (i == position)
                    {
                        j++;
                    }
                    else
                    {
                        tempArr[i] = this.Array[j];
                    }
                    j++;
                }
                this.Array = tempArr;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Выход за пределы ёмкости");
            }

        }
        public void IncreaseCapacity(int n)
        {
            int newCap = Capacity + n;
            T[] tempArr = new T[newCap];
            for (int i = 0; i < Count; i++)
            {
                tempArr[i] = this.Array[i];
            }
            this.Array = tempArr;
        }
        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
        public void Filter(Func<T, bool> del)
        {
            T[] temp = new T[Count];
            int j = 0;
            for (int i = 0; i < Count; i++)
            {
              
                if(del(Array[i]))
                {
                    temp[j] = Array[i];
                    j++;
                }
               
               
            }
            Array = temp;
           
        }

        public void Sort(Func<T, T, int> comparator)
        {

            {
                for (int i = 0; i < Count - 1; i++)
                {
                    for (int j = 0; j < Count - i - 1; j++)
                    {
                        if (comparator(Array[j], Array[j + 1]) > 0)
                        {
                            var temp = Array[j];
                            Array[j] = Array[j + 1];
                            Array[j + 1] = temp;
                        }

                    }
                }
            }
        }

        public void Show()
        {
            for (int i = 0; i < Count; i++)
            {
                Console.WriteLine(Array[i]);
            }
        }
       
    }
}
